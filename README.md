# Code of Conduct

The Rohde & Schwarz code of conduct is currently hosted on [github.com/rohde-schwarz](https://github.com/Rohde-Schwarz/rohde-schwarz/blob/master/code-of-conduct.md) along with Rohde & Schwarz open source software projects.